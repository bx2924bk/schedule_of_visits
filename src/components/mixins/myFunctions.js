export const myFunction = {
    methods: {
        decrease() {
            this.month--;
            if (this.month < 0) {
                this.month = 11;
                this.year--;
            }
        },

        increase() {
            this.month++;
            if (this.month > 11) {
                this.month = 0;
                this.year++;
            }
        },

        dutyToday(year, month, day, studentId) {
            const notAbsent = this.$store.getters.DAYLIST(`${year}-${month}-${day}`)
            if (notAbsent !== undefined) {
                const index = notAbsent.studentsId.findIndex(id => id === studentId)
                if (index !== -1)
                    return true
                else
                    return false
            } else
                return false
        },

        checkedDay(year, month, day, studentId) {
            if ((this.curentDay >= day && this.curentYear >= this.year && this.curentMonth >= this.month)
                || (this.curentYear > this.year))
                this.$store.dispatch('VISITED', {day: `${year}-${month}-${day}`, id: studentId})
        },
    }
}
