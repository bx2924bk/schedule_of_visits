import Vue from 'vue'
import {BootstrapVue, IconsPlugin} from 'bootstrap-vue'
import {store} from "@/store";
import moment from 'moment';
import VueMoment from 'vue-moment';
// Vue.prototype.$moment = moment;

Vue.use(VueMoment, {moment})
// Vue.use(require('vue-moment'));

import router from './router/'

import App from './App.vue'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import './assets/style.scss'

Vue.use(BootstrapVue)
Vue.use(IconsPlugin)

Vue.config.productionTip = false

moment.locale('uk')

new Vue({
    store,
    router,
    render: h => h(App),
}).$mount('#app')
