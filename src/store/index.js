import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';

import students from './modules/students';
import dataList from "./modules/dataList";

Vue.use(Vuex)

export const store = new Vuex.Store({
    state: {},
    getters: {},
    mutations: {},
    actions: {},
    modules: {
        students,
        dataList
    },
    plugins: [createPersistedState()]
});