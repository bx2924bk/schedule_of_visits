const state = {
    students: [
        {
            id: 1,
            name: 'Exemple_1'
        },
        {
            id: 2,
            name: 'Exemple_2'
        }],
};

const getters = {
    STUDENTS: state => {
        return state.students;
    },
    STUDENT: state => id => {
        return state.students.find(item => item.id === id);
    }
};

const mutations = {
    ADD_STUDENTS: (state, payload) => {
        const lastId = state.students.length;
        let id = 1;
        if (lastId !== 0)
            id = state.students[lastId - 1].id + 1;
        state.students.push({id: id, name: payload});
    },
    DELETE_STUDENTS: (state, payload) => {
        const indexById = state.students.findIndex(list => list.id === payload);
        state.students.splice(indexById, 1);
    },
    EDIT_STUDENT: (state, {id, name}) => {
        const indexById = state.students.findIndex(list => list.id === id);
        const editedStudent = state.students.find(list => list.id === id);
        editedStudent.name = name;
        state.students.splice(indexById, 1, editedStudent);
    }
};

const actions = {
    SAVE_STUDENT: (context, payload) => {
        context.commit('ADD_STUDENTS', payload);
    },
    REMOVE_STUDENT: (context, payload) => {
        context.commit('DELETE_STUDENTS', payload);
    },
    CHANGE_STUDENT: (context, payload) => {
        context.commit('EDIT_STUDENT', payload);
    },
};

export default {
    state,
    getters,
    mutations,
    actions,
};