const state = {
    dateOfVisiting: []
};

const getters = {
    VISITING: state => {
        return state.dateOfVisiting;
    },
    DAYLIST: state => thatDay => {
        return state.dateOfVisiting.find(item => item.day === thatDay);
    }
};

const mutations = {
    ADD_VISITED: (state, {day, id}) => {
        const studentId = [];
        studentId[0] = id;
        if (state.dateOfVisiting.length) {
            const indexByDay = state.dateOfVisiting.findIndex(list => list.day === day);
            if (indexByDay !== -1) {
                const indexById = state.dateOfVisiting[indexByDay].studentsId.findIndex(index => index === id)
                if (indexById !== -1) {
                    state.dateOfVisiting[indexByDay].studentsId.splice(indexById, 1);
                } else {
                    state.dateOfVisiting[indexByDay].studentsId.push(id);
                }
            } else {
                state.dateOfVisiting.push({day: day, studentsId: studentId})
            }
        } else {
            state.dateOfVisiting.push({day: day, studentsId: studentId})
        }
    },
    DELETE_STUDENT: (state, payload) => {
        let i = 0
        // console.log(state.dateOfVisiting.length, payload)
        for (i; i < state.dateOfVisiting.length; i++) {
            const deleteId = state.dateOfVisiting[i].studentsId.findIndex(list => list === payload)
            if (deleteId !== -1)
                state.dateOfVisiting[i].studentsId.splice(deleteId, 1);
        }
    },
};

const actions = {
    VISITED: (context, payload) => {
        context.commit('ADD_VISITED', payload);
    },
    DELETE_IN_LIST: (context, payload) => {
        context.commit('DELETE_STUDENT', payload);
    },
};

export default {
    state,
    getters,
    mutations,
    actions,
};