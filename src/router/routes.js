import Students from "@/components/pages/Students";
import StudentInfo from "@/components/pages/StudentInfo";


export default [
    {
        path: '/',
        redirect: '/students'
    },
    {
        path: '/students',
        name: 'students',
        component: Students
    },
    {
        path: '/students/:id',
        name: 'student',
        component: StudentInfo
    },

]
